.. Those need to change according to your own project.
..
.. make sure you review each line and also fix the link in webirc.html
..
.. |project_name| replace:: feed2exec
.. |irc_channel| replace:: ``#feed2exec``
.. |webirc| replace:: https://webchat.freenode.net/?nick=feed2exec.&channels=feed2exec&prompt=1
.. _irc: ircs://irc.freenode.net/feed2exec/
.. _webirc: https://webchat.freenode.net/?nick=feed2exec.&channels=feed2exec&prompt=1
.. _project: https://gitlab.com/anarcat/feed2exec/
.. _merge requests: https://gitlab.com/anarcat/feed2exec/merge_requests
.. _tag: https://gitlab.com/anarcat/feed2exec/tags
.. _issues: https://gitlab.com/anarcat/feed2exec/issues
.. _edited online: https://gitlab.com/anarcat/feed2exec/edit/master/README.rst
